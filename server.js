const express = require("express");
const bodyParser = require("body-parser");
// const axios = require("axios");
const admin = require("firebase-admin");

//arona
//const serviceAccount = require("./dcp-push-notification-dev-firebase-adminsdk-auzwn-a9206bf943.json");
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://dcp-push-notification-dev-default-rtdb.firebaseio.com",
// });
//richard
const serviceAccount = require("./adlfcm-firebase-adminsdk-lyi1f-230842b874.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "https://adlfcm-default-rtdb.asia-southeast1.firebasedatabase.app",
});

const app = express();
const port = process.env.PORT || 5000;

makeHeadRequest = (a) => {
  console.log(a);
  // let res = await axios.head("http://localhost:3000/");
  // console.log(`Status: ${res.status}`);
  // console.log(`Server: ${res.headers.server}`);
  // console.log(`Date: ${res.headers.date}`);
  const payload = {
    notification: {
      title: "Testing",
      body: a,
    },
  };
  console.log(payload);

  admin
    .messaging()
    .sendToTopic("testing", payload)
    .then(function (response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function (error) {
      console.log("Error sending message:", error);
    });
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/api/subscribeToken", (req, res) => {
  const registrationTokens = [
    "fwXOemc37fM4BaZ1DGkads:APA91bHgwpiVSch6agiPeFU2S9cSVdeZx42mGQRZCKHHHkUTo0LiFvLf0N-I-79ILqGehGj4NrxtbjTGWfFi-IIM9pJ1KGTOkXrExAwiyc1hjufF4RYxbzMtyT3TK7cK-gKRZvrso99O",
    "dhzz_PB8rfQhXf7fjZ-8-U:APA91bH4C5vQxyibagroWJB6d0CI-ETNp4fu5cetdJsTLXj0JdWexpeoUqS26ssFexAzo0OTvOXlPcZirVZ042EfMaEa5Vic5a3XpYDgVJn6EZEPXMGOsee4_3bOF7r7nCwfofjkpfNh",
  ];

  let topic = "testing";

  admin
    .messaging()
    .subscribeToTopic(registrationTokens, topic)
    .then(function (response) {
      res.send({ express: response.successCount, subscribeTopic: topic });
      console.log("Successfully subscribed to topic:", response);
    })
    .catch(function (error) {
      res.send({ express: response.errors });
      console.log("Error subscribing to topic:", error);
    });
});

app.post("/api/world", (req, res) => {
  // console.log(req);
  const payload = {
    notification: {
      title: "Testing",
      body: req.body.post,
    },
  };
  console.log(payload);

  admin
    .messaging()
    .sendToTopic("testing", payload)
    .then(function (response) {
      res.send({ express: response });
      console.log("Successfully sent message:", response);
    })
    .catch(function (error) {
      res.send({ express: error });
      console.log("Error sending message:", error);
    });
  // makeHeadRequest(req.body.post);
  // console.log(req.body);
  // makeHeadRequest(req.body.post);
  // res.send(
  //   `I received your POST request. This is what you sent me: ${req.body.post}. Your user token is: ${req.body.fcm_token}`
  // );
});

app.listen(port, () => console.log(`Listening on port ${port}`));
