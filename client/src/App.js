import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: "",
      subscribeTopic: "",
      post: "",
      responseToPost: "",
    };
  }

  componentDidMount() {
    this.callApi()
      .then((res) =>
        this.setState({
          response: res.express,
          subscribeTopic: res.subscribeTopic,
        })
      )
      .catch((err) => console.log(err));
  }

  callApi = async () => {
    const response = await fetch("/api/subscribeToken");
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const response = await fetch("/api/world", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        post: this.state.post,
        fcm_token: localStorage.getItem("fcm_token"),
      }),
    });
    const status = await response?.status;
    const body = await response.text();
    // console.log("full response from node", body.express);
    if (status === 200) {
      this.setState({ responseToPost: body, post: "" });
      alert("notification sent!");
    } else {
      this.setState({ responseToPost: body, post: "" });
      alert("failed to sent notification!");
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        {this.state.response !== "" ? (
          <p>
            {" "}
            {this.state.response} token are subscribed to "
            {this.state.subscribeTopic}" topic
          </p>
        ) : (
          ""
        )}

        <form onSubmit={this.handleSubmit}>
          <p>
            <strong>Broadcast message</strong>
          </p>
          <input
            type="text"
            value={this.state.post}
            onChange={(e) =>
              this.setState({
                post: e.target.value,
              })
            }
          />
          <button type="submit">Submit</button>
        </form>
        <p>
          <strong>Server response to post:</strong>
        </p>
        <p>{this.state.responseToPost}</p>
      </div>
    );
  }
}

export default App;
