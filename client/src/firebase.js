import firebase from "firebase";

// custom testing app arona
// const firebaseConfig = {
//   apiKey: "AIzaSyCrNFE2lsGqcozqKVO_HK0XIYRPpO9enFw",
//   authDomain: "dcp-push-notification-dev.firebaseapp.com",
//   databaseURL: "https://dcp-push-notification-dev-default-rtdb.firebaseio.com",
//   projectId: "dcp-push-notification-dev",
//   storageBucket: "dcp-push-notification-dev.appspot.com",
//   messagingSenderId: "111274413116",
//   appId: "1:111274413116:web:40149aec6b7fc72c2c0739",
// }

// custom testing app richard
const firebaseConfig = {
  apiKey: "AIzaSyBL51Up3CFTi8VwldetA_nEyVU2txZIlQE",
  authDomain: "adlfcm.firebaseapp.com",
  databaseURL:
    "https://adlfcm-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "adlfcm",
  storageBucket: "adlfcm.appspot.com",
  messagingSenderId: "940805143673",
  appId: "1:940805143673:web:a364506680e799db31a8a8",
  measurementId: "G-2HNW7VH6S5",
};

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);

  navigator.serviceWorker
    .register("/firebase-messaging-sw.js")
    .then((registration) => {
      firebase.messaging().useServiceWorker(registration);
    });
};

export const askForPermissioToReceiveNotifications = async () => {
  //vapidKey arona : BL1Z7Wngsood6qdU_da_CRAg5bHl_4D1pD_elHEuRpLIbkF5x9xcj91TucFCKqI6_w-PHVuWIL6SDRmz6MOMx0g
  //vapidKey richard : BAuZCPW7OKCR1Kwedi2ox5G_9V2-c0q2Uyyi_XzqD5fnqhT6KsE-ks8JJFxE3rWTE_H1YJYp_QcOtoxFjb5xTWU
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken({
      vapidKey:
        "BAuZCPW7OKCR1Kwedi2ox5G_9V2-c0q2Uyyi_XzqD5fnqhT6KsE-ks8JJFxE3rWTE_H1YJYp_QcOtoxFjb5xTWU",
    });
    console.log("user token: ", token);
    if (token !== "undefined") {
      localStorage.setItem("fcm_token", token);
    }
    return token;
  } catch (error) {
    localStorage.setItem("fcm_token", "no permission");
    console.error(error);
  }
};
