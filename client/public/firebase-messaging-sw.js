/* eslint-disable no-undef */
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts("https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.3.0/firebase-messaging.js");

// custom testing app arona
// const firebaseConfig = {
//   apiKey: "AIzaSyCrNFE2lsGqcozqKVO_HK0XIYRPpO9enFw",
//   authDomain: "dcp-push-notification-dev.firebaseapp.com",
//   databaseURL: "https://dcp-push-notification-dev-default-rtdb.firebaseio.com",
//   projectId: "dcp-push-notification-dev",
//   storageBucket: "dcp-push-notification-dev.appspot.com",
//   messagingSenderId: "111274413116",
//   appId: "1:111274413116:web:40149aec6b7fc72c2c0739",
// }

// custom testing app richard
const firebaseConfig = {
  apiKey: "AIzaSyBL51Up3CFTi8VwldetA_nEyVU2txZIlQE",
  authDomain: "adlfcm.firebaseapp.com",
  databaseURL:
    "https://adlfcm-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "adlfcm",
  storageBucket: "adlfcm.appspot.com",
  messagingSenderId: "940805143673",
  appId: "1:940805143673:web:a364506680e799db31a8a8",
  measurementId: "G-2HNW7VH6S5",
};

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
