var admin = require("firebase-admin");

var serviceAccount = require("./dcp-push-notification-dev-firebase-adminsdk-auzwn-a9206bf943.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dcp-push-notification-dev-default-rtdb.firebaseio.com",
});
