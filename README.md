# pushnotifmanager

In notifmanager folder, there is two major code part. The first one is the SERVER under path "/notifmanager" and the second one is the CLIENT under path "/notifmanager/client". Point the command prompt directory to correct location and check if you can find "package.json" there. There must be 2 of them, one under path "/notifmanager" and "/notifmanager/client". After that, install the depedencies using "npm install" and wait the process to complete.

# how to run?

Open 2 command prompt

on command prompt #1 do:

1. go to "/notifmanger" directory.
2. use "npm run server" to run the server.

on command prompt #2 do:

1. go to "/notifmanger/client" directory.
2. use "npm run client" to run the server.
